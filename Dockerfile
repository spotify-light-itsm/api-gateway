FROM maven:3.6.3-jdk-11-slim AS MAVEN_BUILD
ARG SPRING_ACTIVE_PROFILE
MAINTAINER Alpha
COPY pom.xml /build/
COPY src /build/src/
WORKDIR /build/
RUN mvn clean install -Dspring.profiles.active=$SPRING_ACTIVE_PROFILE && mvn package -B -e -Dspring.profiles.active=$SPRING_ACTIVE_PROFILE


FROM openjdk:11-slim
EXPOSE 9090
WORKDIR /app
COPY --from=MAVEN_BUILD /build/target/sst-alpha-api-gateway.jar /app/sst-alpha-api-gateway.jar
COPY ./src/wait-for-it.sh .
RUN chmod +x wait-for-it.sh

ENV EUREKA_HOST=${EUREKA_HOST:-"localhost:8761"}

ENTRYPOINT ./wait-for-it.sh ${EUREKA_HOST} -- java -jar /app/sst-alpha-api-gateway.jar
