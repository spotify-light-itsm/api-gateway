package sst.alpha.apigateway.services;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.Jwts;
import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.PublicKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.X509EncodedKeySpec;
import java.util.Base64;

@Service
public class JwtServiceImpl implements JwtService {

    private String publicKey="MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAlEsTCg+q8hKdPEYCSndiqJ2TL0sbK4n9oc6Q+WfZavpLIwx2yVr0tsu9TfuzVLjx8WD4z7MjhjxKf9eMTJtWICD37Pts4vtJdtpJSFNeh20vgYMRnN6aCast7jlnY3ws7N+5FZwh0JwDoBTgvbscT03OOSLMzKTUudlobNqx6V8sTEkGEo300h48rwsNmJDJfiL681r28uLFIlbN6vYI5qyS0UChNWJBy1X5IxUSko1bnMMW2jCJO+inf+CKs6DxbMJ/c0JtwxlTPQ6KGaa4Zggw2BeWu6N/rptKTr/bbHQVBqRD4N8dtGMM6PIQsafXbW+XjGbimGAQfw/d4PWQ7wIDAQAB";

    @Override
    @SneakyThrows
    public Jws<Claims> parseAndValidate(String jwt) {
        PublicKey publicKey = null;
        try {
            publicKey = getPublicKey();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }

        return Jwts.parser().setSigningKey(publicKey).parseClaimsJws(jwt);
    }

    private PublicKey getPublicKey() throws NoSuchAlgorithmException, InvalidKeySpecException {
        String preProcessedKey = publicKey.replaceAll("\n", "");
        byte[] key = Base64.getDecoder().decode(preProcessedKey);
        X509EncodedKeySpec keySpec = new X509EncodedKeySpec(key);
        KeyFactory keyFactory = KeyFactory.getInstance("RSA");
        return keyFactory.generatePublic(keySpec);
    }

}
